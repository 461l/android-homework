package com.example.oursun;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    String lat;
    String lng;
    Intent nextPage;

    final String MAP_API_KEY = "AIzaSyBSFH6oIXyzVJ6EKUag7qRxIkeGQLjBI-g";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        nextPage = new Intent(this, MapAndWeather.class);
    }

    public void button(View view){
        EditText inp = (EditText) findViewById(R.id.editText);
        new GetCoordinates().execute(inp.getText().toString().replace(" ", "+"));
    }

    private class GetCoordinates extends AsyncTask<String, Void, String> {
        ProgressDialog dialog = new ProgressDialog(MainActivity.this);

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            dialog.setMessage("Please wait");
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }


        @Override
        protected String doInBackground(String... strings){
            String response;
            try{
                String address = strings[0];
                HTTPRequestHandler http = new HTTPRequestHandler();
                String url = String.format("https://maps.googleapis.com/maps/api/geocode/json?address=%s&key=%s", address, MAP_API_KEY);
                response = http.getHTTPData(url, false);
                return response;
            } catch(Exception e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s){
            try{
                JSONObject jsonObject = new JSONObject(s);

                lat = ((JSONArray)jsonObject.get("results")).getJSONObject(0).getJSONObject("geometry").getJSONObject("location").get("lat").toString();
                lng = ((JSONArray)jsonObject.get("results")).getJSONObject(0).getJSONObject("geometry").getJSONObject("location").get("lng").toString();

                if(dialog.isShowing()) dialog.dismiss();

                nextPage.putExtra("lat", lat);
                nextPage.putExtra("lng", lng);

                startActivity(nextPage);

            } catch (JSONException e){

            }
        }
    }
}
