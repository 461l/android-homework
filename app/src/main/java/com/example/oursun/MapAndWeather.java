package com.example.oursun;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

public class MapAndWeather extends AppCompatActivity implements OnMapReadyCallback {
    String lat = "0", lng = "0";
    String temp, hum, ws, precType, precProb;
    final String DS_API_KEY = "c0d970e27bb068527549553cc4d9907e";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_and_weather);
        geocode();
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        new GetWeather().execute();
    }

    public void geocode(){
        Intent i = getIntent();
        Bundle b = i.getExtras();
        lat = b.getString("lat");
        lng = b.getString("lng");
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLng location = new LatLng(Double.parseDouble(lat), Double.parseDouble(lng));
        googleMap.addMarker(new MarkerOptions().position(location).title("Your Current Location"));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(location));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(12));
        //Toast.makeText(this, "I'm a good boy!", Toast.LENGTH_SHORT).show();
    }


    private class GetWeather extends AsyncTask<String, Void, String> {
        ProgressDialog dialog = new ProgressDialog(MapAndWeather.this);

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            dialog.setMessage("Please wait");
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }


        @Override
        protected String doInBackground(String... strings){
            String response;
            try{
                HTTPRequestHandler http = new HTTPRequestHandler();
                String url = String.format("https://api.darksky.net/forecast/c0d970e27bb068527549553cc4d9907e/%s,%s", lat, lng);
                response = http.getHTTPData(url, true);
                return response;
            } catch(Exception e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s){
            try{
                JSONObject jsonObject = new JSONObject(s);

                JSONObject currently = jsonObject.getJSONObject("currently");

                temp = currently.get("temperature").toString();
                hum = currently.get("humidity").toString();
                ws = currently.get("windSpeed").toString();
                precProb = currently.get("precipProbability").toString();
                Double percentage = Double.parseDouble(precProb) * 100.0;
                if(percentage > 0.0)
                    precType = currently.get("precipType").toString();
                else
                    precType = "";

                if(dialog.isShowing()) dialog.dismiss();

                TextView tl = (TextView) findViewById(R.id.TempResult);
                tl.setText(temp + " F");
                TextView hl = (TextView) findViewById(R.id.HumResult);
                hl.setText(Double.parseDouble(hum)*100 + "%");
                TextView wsl = (TextView) findViewById(R.id.WindResult);
                wsl.setText(ws + " mph");
                TextView pl = (TextView) findViewById(R.id.PrecResult);
                pl.setText(percentage + "% " + precType);

            } catch (JSONException e){

            }
        }
    }

}
